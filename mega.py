#!/usr/bin/env python
# encoding=utf8
# made by zodman
import click
import os.path
import os
import ConfigParser
from shelljob import proc
import tempmail
import time
import pipes

LOGIN = "animerespaldomega123"
DOMAIN = "p33.org"
EMAIL = '%s{}%s' % (LOGIN, DOMAIN)

def get_megacount():
    home = os.environ.get("HOME")
    parser = ConfigParser.ConfigParser()
    parser.read(os.path.join(home, ".megarc"))
    parser.get("Login","_count")
    return int(parser.get("Login","_count")), parser.get("Login","Password")

def write_megarc(count, password, email):
    home = os.environ.get("HOME")
    cfg = ConfigParser.ConfigParser()
    cfg.optionxform = str
    cfg.add_section("Login") 
    cfg.set("Login", "Username", email)
    cfg.set("Login", "Password", password)
    cfg.set("Login", "_count", str(count))
    f = open(os.path.join(home, ".megarc"), "w")
    cfg.write(f)
    f.close()



def verify_account(email, cmd, login, domain):
    login,_ = email.split("@")
    tm = tempmail.TempMail(login=login, domain=domain)
    activation_link=tm.get_mailbox()[0].get("mail_text").split("\n")
    alink = activation_link[6]
    print "activation code {}".format(alink)
    if alink:
        cmd_ = cmd.strip().split(" ")
        cmd_[-2] = "'{}'".format(pipes.quote(cmd_[-2]))
        cmd_[0] = "megareg"
        cmd = " ".join(cmd_)
        #print "cmd: {}".format(cmd)
        cmd = cmd.replace("@LINK@", pipes.quote("{}".format(alink)))
        #click.echo("executing {}".format(cmd))
        #click.echo(cmd)
        resp=proc.call(cmd)
        click.echo(resp)


@click.group()
def mega():
    pass

@mega.command()
@click.option("--login", default=LOGIN)
@click.option("--domain", default=DOMAIN)
def register(login,domain):
    """ register email accont from tempemail"""
    count, password = get_megacount()
    count +=1
    email = "{}{}@{}".format(login,count, domain)
    print "email: {}".format(email)
    cmd = [
    'megareg',
    '--name=AndresVargas',
    '-e', email,
    '-p', password,
    '--register',
    '--scripted',
    ]
    click.echo("Registrando ==== {}".format(email))
    res = proc.call(cmd)
    click.echo(res)
    write_megarc(count, password, email)
    click.echo(" tu megarc ah cambiado")
    time.sleep(5)
    verify_account(email, res, login, "@{}".format(domain))
    print "check at: http://temp-mail.ru"
    print "login: {}".format(email.split("@")[0])
    print "domain: {}".format(domain)


if __name__== "__main__":
    mega()
